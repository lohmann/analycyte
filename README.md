
# analycyte <img src="man/www/hex-analycyte-pipe-2.png" align="right" alt="" width="120" />

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

## Installation

To use the R package `{analycyte}`, it is essential to first install two
complementary packages: `{analycyte.utils}` and `{analycyte.projects}`.
The analycyte.utils package is necessary, as it contains the functions
needed to generate reports and interact with the graphical interface. As
for the analycyte.projects package, it provides access to the databases
associated with each project and contains the various analysis report
templates.

``` r
remotes::install_git("https://gitlab.in2p3.fr/eugenie.lohmann/analycyte.utils")
remotes::install_git("https://gitlab.in2p3.fr/eugenie.lohmann/analycyte.projects")
```

Once these packages have been installed, you can obtain the development
version of analycyte with :

``` r
remotes::install_git("https://gitlab.in2p3.fr/eugenie.lohmann/analycyte")
```

## Documentation

We’ve started writing documentation, which you can find at:
<https://eugloh.quarto.pub/analycyte/>

## Deployement

You can deploy the application (and its dependencies) using Docker and
the following git repo:
<https://gitlab.in2p3.fr/eugenie.lohmann/deploy>

## Contact

analycyte is still under development. If you would like to send us :

- bug reports

- support requests

- suggestions for improvement

- specific documentation requirements

- collaboration proposals

or anything else, please don’t hesitate to contact us.

Eugénie Lohmann: <eugenie.lohmann@inserm.fr>

Samuel Granjeaud: <samuel.granjeaud@inserm.fr>

## Code of Conduct

Please note that the analycyte project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
