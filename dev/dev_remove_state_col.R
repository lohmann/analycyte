readRDS("param_list.rds") -> param_list
r_global <- list()
r_global$data <- analycyte.utils::create_se( # presuppose que les tables sont dans le meme ordre, a modifier
  list_formats = param_list[["list_formats"]],
  abundance = param_list[["abundance"]],
  mfi = param_list[["mfi"]],
  is_scaled = param_list[["is_scaled"]],
  to_scale = param_list[["to_scale"]],
  meta = param_list[["meta"]],
  color = param_list[["color"]],
  markers = param_list[["markers"]],
  target_long_columns = param_list[["target_long_columns"]],
  fcs_ids = param_list[["fcs_ids"]],
  parsing = param_list[["parsing"]],
  grouping_colname = param_list[["grouping_colname"]],
  applied_fct = param_list[["applied_fct"]],
  asinh_Ab = param_list[["asinh_Ab"]],
  column_with_ncells = param_list[["column_with_ncells"]],
  fcs_split = param_list[["fcs_split"]],
  is_percent = param_list[["is_percent"]],
  asinh_MFI = param_list[["asinh_MFI"]],
  state_marker = param_list[["state_marker"]],
  sample_rename = param_list[["sample_rename"]],
  omiq_column_name = param_list[["omiq_column_name"]],
  annot_markers = param_list[["annot_markers"]]
)


