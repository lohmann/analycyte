readRDS("/home/lohmann/analycyte-projects/IMMUNOCOL/094EE058/dataFE.rds")-> parameters

# library(SummarizedExperiment)
# require(SummarizedExperiment)
require(analycyte.utils)
colData(parameters$SE_mfi) |> rownames() # fcs 
rowData(parameters$SE_mfi) |> rownames() # clusters
getFeature(parameters$SE_mfi,c("metadata","annot_markers")) -> annot_markers

annot_markers$Marker <- rownames(annot_markers)

getFeature(parameters$SE_mfi,c("assays")) -> assays




long_assays_median <- do.call(
  dplyr::bind_rows,
  lapply(names(assays), function(name) {
    assays[[name]] |>
      t() |>
      as.data.frame( check.names=F) |> 
      tidyr::pivot_longer(
        cols = everything(),
        names_to = "cluster",
        values_to = "value"
      ) |>
      dplyr::group_by(cluster) |>
        dplyr::summarise(dplyr::across(everything(), median), .groups = "drop") |>
      dplyr::mutate(Marker = name) # Add the assay name
  })
) |> na.omit()

long_assays <- do.call(
  dplyr::bind_rows,
  lapply(names(assays), function(name) {
    assays[[name]] |>
      t() |>
      as.data.frame( check.names=F) |> 
      tidyr::pivot_longer(
        cols = everything(),
        names_to = "cluster",
        values_to = "value"
      ) |>
      # dplyr::group_by(cluster) |>
      #   dplyr::summarise(dplyr::across(everything(), median), .groups = "drop") |>
      dplyr::mutate(Marker = name) # Add the assay name
  })
) |> na.omit()

library(ggplot2)

list_marker <-c( "CD16" ,  "CD11c"  ,"CD4"  , "BTN2"   ,"Vd2"  ,  "CD11b")

    ggplot(long_assays_median|> dplyr::filter(Marker%in%list_marker), aes(x = value)) +
      geom_histogram(bins = 50) + # Add points
      facet_wrap(~Marker, ncol=1) + # Facet by 'group'
      geom_vline(data = annot_markers|> dplyr::filter(Marker%in%list_marker), aes(xintercept = `max value`), 
                 color = "red", linetype = "dashed") + # Add vertical lines
                  geom_vline(data = annot_markers|> dplyr::filter(Marker%in%list_marker), aes(xintercept = `min value`), 
                color = "blue", linetype = "dashed") + # Add vertical lines
      theme_minimal()
