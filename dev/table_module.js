function table_module_js(ns_prefix) {
  
  $("#" + ns_prefix + "table_tb" ).on("click", ".delete_btn", function() {
    console.log("Delete button clicked, ID:", this.id); // Debug message
    Shiny.setInputValue(ns_prefix + "id_to_delete" , this.id, { priority: "event"});
    $(this).tooltip('hide');
  });
  
$("#" + ns_prefix + "table_tb").on("click", ".edit_btn", function() {
  console.log("Edit button clicked, ID:", this.id); // Debug message
  Shiny.setInputValue(ns_prefix + "id_to_edit", this.id, { priority: "event"});
  $(this).tooltip('hide');
});

}
function updateColumnCheckboxes(columnClass, check) {
  $('.' + columnClass).each(function() {
    $(this).prop('checked', check).trigger('change');
  });
}

var stateToggle = true;
$(document).on('click', '#toggleState', function() {
  updateColumnCheckboxes('state', stateToggle);
  stateToggle = !stateToggle;
});

var typeToggle = true;
$(document).on('click', '#toggleType', function() {
  updateColumnCheckboxes('type', typeToggle);
  typeToggle = !typeToggle;
});

$(document).on('change', '.checkbox-group', function() {
  var group = $(this).attr('name');
  if (this.checked) {
    $('input[name="' + group + '"]').not(this).prop('checked', false);
  }
  var result = {};
  $('.checkbox-group').each(function() {
    var name = $(this).attr('name');
    if (!result[name]) {
      result[name] = [];
    }
    if ($(this).is(':checked')) {
      result[name].push($(this).val());
    }
  });
  Shiny.setInputValue('checkboxValues', result, {priority: 'event'});
});
