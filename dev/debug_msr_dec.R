path <- "/home/lohmann/cuisine.analycyte/MSR decembre"
rio::import(file.path(path,"MEL-PD1 panel T-NK MEDIAN.csv")) -> imported_mfi
rio::import(file.path(path,"MEL-PD1 panel T-NK MC.csv")) -> imported_metadata
rio::import(file.path(path,"MEL-PD1 panel T-NK COUNT.csv")) -> imported_abun


defaultValue <- list(
  clustCol = "filter",
  markerCol = "ftr (sec)",
  mfiValueCol = "value",
  sharedCol = "file",
  totalCell = "Unfiltered|count",
  fcs_names = "patient",
  ppatient = 1,
  pcluster = 1,
  is_scaled = T,
  to_scaled = F,
  fcs_to_cut = F,
  alternative_name = F
)

params_list <-  list(
    "list_formats" = list(
      "abundance" = "wide", # "abundance" = input$format_abund,
      "mfi" = "long", # "mfi" =  input$format_mfi,
      "metadata" = "wide", # "metadata" = input$format_metadata,
      "markers" = NULL
    ),
    "grouping_colname" = list(
      "abundance" = defaultValue$sharedCol,
      "mfi" = defaultValue$sharedCol,
      "metadata" = defaultValue$sharedCol,
      "markers" = NULL
    ),
    "parsing" = list(
      sep = "#",
      pos_ids = 1
    ),
    "target_long_columns" = list(
      "value" =  defaultValue$sharedCol,
      "cluster" = defaultValue$clustCol,
      "marker" = defaultValue$markerCol,
      "mfi" = defaultValue$mfiValueCol
    ),
    "fcs_ids" = "patient", #        "fcs_ids" = input$fcs_names,
    # "abundance" = imported_abun$data() |> dplyr::arrange(!!sym(input$sharedColAbundance)),
    "abundance" = imported_abun, #imported_abun$data(),
    "mfi" = imported_mfi, #imported_mfi$data(),
    "is_scaled" =  defaultValue$is_scaled,
    "to_scale" =  defaultValue$to_scaled,
    "meta" = imported_metadata,# imported_metadata$data(),
    "color" = NULL,
    "markers" = NULL,
    "applied_fct" = "asinh", #         "applied_fct" =  input$transformation_fct,
    "asinh_Ab" = 0.03, #         "asinh_Ab" = input$Ab_aSinh,
    "fcs_split" = FALSE, #        "fcs_split" = input$check_remove_fcs_labs,
    "is_percent" = F,
    "asinh_MFI" = 5,
    "state_marker" = NULL,
    "omiq_column_name" = TRUE, #        "omiq_column_name" = input$check_is_colname_omiq,
    "column_with_ncells" = defaultValue$totalCell,
    "sample_rename" = NULL
    # ,"annot_markers" = table_markers()
  )


r_global <- list()

r_global$data <- analycyte.utils::create_se( # presuppose que les tables sont dans le meme ordre, a modifier
  list_formats = params_list[["list_formats"]],
  abundance = params_list[["abundance"]],
  mfi = params_list[["mfi"]],
  is_scaled = params_list[["is_scaled"]],
  to_scale = params_list[["to_scale"]],
  meta = params_list[["meta"]],
  color = params_list[["color"]],
  markers = params_list[["markers"]],
  target_long_columns = params_list[["target_long_columns"]],
  fcs_ids = params_list[["fcs_ids"]],
  parsing = params_list[["parsing"]],
  grouping_colname = params_list[["grouping_colname"]],
  applied_fct = params_list[["applied_fct"]],
  asinh_Ab = params_list[["asinh_Ab"]],
  column_with_ncells = params_list[["column_with_ncells"]],
  fcs_split = params_list[["fcs_split"]],
  is_percent = params_list[["is_percent"]],
  asinh_MFI = params_list[["asinh_MFI"]],
  state_marker = params_list[["state_marker"]],
  sample_rename = params_list[["sample_rename"]],
  omiq_column_name = params_list[["omiq_column_name"]],
  annot_markers = params_list[["annot_markers"]]
)


# tests
# Code dplyr: 
# fcs_data %>% filter(Unfiltered.count >= 125L & Unfiltered.count <= 150L)
# Expression: 
# Unfiltered.count >= 125L & Unfiltered.count <= 150L
# 
# Code dplyr:
#   
#   fcs_data %>% filter(!(Response %in% "D")) %>% filter(!(shared_col %in% 
#                                                            c("124.fcs", "HV46.fcs")))
# 
# Expression:
#   
#   !(Response %in% "D") & !(shared_col %in% c("124.fcs", "HV46.fcs"
#   ))
# 
# Code dplyr:
#   
#   fcs_data %>% filter(!(Batch %in% c("Batch-6", "Batch-7", "Batch-2"
#   )))
# 
# Expression:
#   
#   !(Batch %in% c("Batch-6", "Batch-7", "Batch-2"))

# (dataFE <- r_global$data)

# use appli's one
(readRDS("/home/lohmann/analycyte-projects/px/094C895C/dataFE.rds") -> dataFE)

(fcs_data <- dataFE$SE_abundance@colData |> as.data.frame())
(clusters_data <- dataFE$SE_abundance@elementMetadata |> as.data.frame())

updated_meta_fcs <- fcs_data %>% dplyr::filter(Unfiltered.count >= 125L & Unfiltered.count <= 150L)
samples <- updated_meta_fcs[["shared_col"]]
updated_meta_clusters <- clusters_data %>%  dplyr::filter(n_cells >= 1420L & n_cells <= 2838L)
clusters <- updated_meta_clusters[["cluster_id"]]

updated_dataFE <- analycyte.utils::filter_feature_set(dataFE = dataFE, samples = samples)
updated_dataFE <- filter_feature_set2(dataFE = dataFE, samples = samples, clusters = clusters)




# apply_dplyr_command <- function(table, dplyr_command) {
#   
#   if (!require(dplyr, quietly = TRUE)) {
#     stop("The 'dplyr' package is required but not installed.")
#   }
# 
#   full_command <- paste0("table", dplyr_command)
# 
#   result <- eval(parse(text = full_command))
#   
#   return(result)
# }



filter_feature_set <- function(
    
  dataFE, 
    t_metadata = "meta",
    scaled_MFI = "scaled_MFI",
    trend_method_ = "none",
    norm_factors_ = "TMM",
    id = "shared_col"
   # , dplyr=  '%>% filter(!(Response %in% "D")) %>% filter(!(shared_col %in% c("124.fcs", "HV46.fcs")))'
  
  ){
  

}



# 
# se <- dataSE$SE_abundance
# 
# se_mfi <- dataSE$SE_mfi
# 
# t_metadata <- "meta"
# scaled_MFI <- "scaled_MFI"
# trend_method_ <- "none"
# norm_factors_ <- "TMM"
# id <- "shared_col"

