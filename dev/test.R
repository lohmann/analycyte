# library(shiny)
# library(DiagrammeR)
#
# ui <- fluidPage(tags$head(tags$script(HTML("
#   $(document).on('click', '.node', function() {
#     Shiny.onInputChange('clicked_node', this.textContent);
#   });
# "))),
#   numericInput("Pvalue","pval:", 0.05, min = 0, max = 1, step=0.001),
#   actionButton("button1", "Ajouter A-->B"),
#   actionButton("button2", "Ajouter B-->D"),
#   actionButton("button3", "Ajouter B-->C"),
#   uiOutput("diagram")
# )
#
# server <- function(input, output, session) {
#   diagramText <- reactiveVal("graph LR\n")
#
#   # observeEvent(input$button1, {
#   #   diagramText(paste0(diagramText(), "A-->B\n"))
#   # })
#   #
#   # observeEvent(input$button2, {
#   #   diagramText(paste0(diagramText(), "B-->D\n"))
#   # })
#   #
#   # observeEvent(input$button3, {
#   #   diagramText(paste0(diagramText(), "B-->C\n"))
#   # })
#
#
#
#   observeEvent(list(input$button1,
#                     input$button2,
#                     input$button3)
#                , {
#     pvalue <- reactive(input$Pvalue) # insérez ici le code pour calculer la valeur de pvalue en fonction de vos données
#
#
#
#       diagramText(paste0(diagramText(), "A-->|pvalue = ", pvalue() ,"|B\n"))
#   })
#
#     output$diagram <- renderUI({
#     mermaid(diagramText())
#   })
#
# }
#
# shinyApp(ui = ui, server = server)
