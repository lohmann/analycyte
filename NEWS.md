# analycyte 3.0.6

* session$reload() when deleting a project 
   
# analycyte 3.0.31

## BUG REPORT

## major change

* avoid observevent overlapping by destroy() olds one
* shared col on import concat check before running
* avoid crash when selecting wrong gcs file in mod import concat

## minor change

* allowing comment of action steps 
* add edit button
* reallow add button
* annotation in da module 
* notification failure dissociation in mod import concat 

note:
will need to add esquisse to the dockerfile base

# analycyte 3.0.3

## major change

* hyperlink to open the html through app
* folder_description.xlsx base file with relative path to html file
* comment column usage is replace by "hyperlink", removal of "path" in the step table

## minor change

* add comment name of the output file
* req for conditions in the da/ds reports


note: 

- archive check box is not working.

# analycyte 3.0.11

* General version 3.0.11 accross analycyte-verse 

# analycyte 3.0.08

* reactiveValuesToList resolves multiple running modules 

# analycyte 3.0.07

* Visual changes on modules and app color

# analycyte 3.0.06

* Welcome dashboard 
* default show the report in the right side of the screen

# analycyte 3.0.05

remove closed modules with removeUI remove_shiny_inputs remove_observers

# analycyte 3.0.0

* rename analyticyte -> analycyte
* rename projectsetup -> analycyte.projects
* rename AnalytiCFX -> analycyte.utils
