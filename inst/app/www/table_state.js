// Function to update checkboxes in a specific column
function updateColumnCheckboxes(columnClass, check) {
  $('.' + columnClass).each(function() {
    $(this).prop('checked', check).trigger('change');
  });
}

// Independent toggle for "state" checkboxes
var stateToggle = true;
$(document).on('click', '#toggleState', function() {
  updateColumnCheckboxes('state', stateToggle);
  stateToggle = !stateToggle;
});

// Independent toggle for "type" checkboxes
var typeToggle = true;
$(document).on('click', '#toggleType', function() {
  updateColumnCheckboxes('type', typeToggle);
  typeToggle = !typeToggle;
});
