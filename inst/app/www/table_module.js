function table_module_js(ns_prefix) {
  
  $("#" + ns_prefix + "table_tb" ).on("click", ".delete_btn", function() {
    console.log("Delete button clicked, ID:", this.id); // Debug message
    Shiny.setInputValue(ns_prefix + "id_to_delete" , this.id, { priority: "event"});
    $(this).tooltip('hide');
  });
  
$("#" + ns_prefix + "table_tb").on("click", ".edit_btn", function() {
  console.log("Edit button clicked, ID:", this.id); // Debug message
  Shiny.setInputValue(ns_prefix + "id_to_edit", this.id, { priority: "event"});
  $(this).tooltip('hide');
});

}


