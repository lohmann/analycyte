#' prettyDT
#'
#' @description DT base
#'
#' @param x x
#' @param ... else
#' @param l length
#' @param extensions_ extensions
#' @param drawCallback JS
#' @param orderable_ boolean TRUE/FALSE
#' @param escape_ TRUE, FALSE, or vector
#'
#' @return DT object
#'
#' @noRd
prettyDT <- function(
    x,
    l = 10,
    extensions_ = c("Buttons"),
    drawCallback = NULL,
    orderable_ = FALSE,
    escape_ = -1,
    ...) {
  tb <- DT::datatable(
    x,
    # rownames = FALSE,
    # class = "compact stripe row-border display nowrap",
    class = list(
      stripe = FALSE,
      compact = TRUE,
      nowrap = TRUE
    ),
    # escape = FALSE,
    escape = escape_,
    options = list(
      pageLength = l,
      scrollX = TRUE,
      # autoWidth = TRUE,
      dom = "Bftip", #' Blfrtip',
      columnDefs = list(
        list(
          # name = names(x),
          className = "dt-center",
          targets = "_all",
          # targets = 0,
          orderable = orderable_
        )
      ),
      drawCallback = drawCallback
    ),
    extensions = extensions_,
    # extensions = c("ColReorder","Buttons"),#,"Select"),
    # callback = JS(js),
    # selection = "multiple",
    # class = 'd-flex text-center',
    # filter = "top",
    ...
  )
  return(tb)
}



# https://github.com/jonas-hag/dynamic_shiny_modules/blob/main/03_additional_information/example_destroy_observeevent.R
remove_shiny_inputs <- function(id, .input) {
  invisible(
    lapply(grep(id, names(.input), value = TRUE), function(i) {
      .subset2(.input, "impl")$.values$remove(i)
    })
  )
}

remove_observers <- function(id, .session) {
  invisible(
    lapply(
      grep(id, names(.session$userData), value = TRUE),
      function(i) {
        .subset2(.session$userData, i)$destroy()
      }
    )
  )
}




write_gct <- function(data_matrix, row_metadata = NULL, col_metadata = NULL, file, col_row_desc = "fcs/cluster") {
  n_rows <- nrow(data_matrix)
  n_cols <- ncol(data_matrix)
  n_row_metadata <- if (!is.null(row_metadata)) ncol(row_metadata) else 0
  n_col_metadata <- if (!is.null(col_metadata)) ncol(col_metadata) else 0

  conn <- file(file, "w")
  on.exit(close(conn)) # Ensure the file is closed on exit

  writeLines("#1.3", conn)
  writeLines(paste(n_rows, n_cols, n_row_metadata, n_col_metadata, sep = "\t"), conn)

  col_meta_header <- c(col_row_desc, colnames(row_metadata), colnames(data_matrix))
  writeLines(paste(col_meta_header, collapse = "\t"), conn)

  reorder_metadata <- function(metadata, rownames_metadata, target_order) {
    matched_indices <- match(target_order, rownames_metadata)
    if (any(is.na(matched_indices))) {
      stop("Mismatch between data and metadata rows/columns.")
    }
    metadata[matched_indices, , drop = FALSE]
  }

  if (n_col_metadata > 0) {
    col_key <- strsplit(col_row_desc, "/")[[1]][1]
    if (col_key == "cluster" && "cluster_id" %in% colnames(col_metadata)) {
      rownames_col_metadata <- as.character(col_metadata[["cluster_id"]])
    } else if (!is.null(rownames(col_metadata))) {
      rownames_col_metadata <- rownames(col_metadata)
    } else {
      stop("Cannot order column annotation")
    }
    reorder_col_metadata <- reorder_metadata(col_metadata, rownames_col_metadata, colnames(data_matrix))

    for (i in seq_len(n_col_metadata)) {
      col_meta_row <- c(colnames(reorder_col_metadata)[i], rep(NA, n_row_metadata), as.character(reorder_col_metadata[, i]))
      writeLines(paste(col_meta_row, collapse = "\t"), conn)
    }
  }

  if (n_row_metadata > 0) {
    row_key <- strsplit(col_row_desc, "/")[[1]][2]
    if (row_key == "cluster" && "cluster_id" %in% colnames(row_metadata)) {
      rownames_row_metadata <- as.character(row_metadata[["cluster_id"]])
    } else if (!is.null(rownames(row_metadata))) {
      rownames_row_metadata <- rownames(row_metadata)
    } else {
      stop("Cannot order row annotation")
    }
    reorder_row_metadata <- reorder_metadata(row_metadata, rownames_row_metadata, rownames(data_matrix))

    for (i in seq_len(n_rows)) {
      row_data <- c(rownames(data_matrix)[i], reorder_row_metadata[i, ], data_matrix[i, ])
      writeLines(paste(row_data, collapse = "\t"), conn)
    }
  } else {
    for (i in seq_len(n_rows)) {
      row_data <- c(rownames(data_matrix)[i], data_matrix[i, ])
      writeLines(paste(row_data, collapse = "\t"), conn)
    }
  }
}

# Symmetric difference function
symmetric_diff <- function(x, y) {
  union(setdiff(x, y), setdiff(y, x))
}
