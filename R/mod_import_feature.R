#' import_feature UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @import shiny
#' @importFrom shinyWidgets awesomeCheckbox actionBttn pickerInput
#' @import shinydashboard
#' @importFrom shinycssloaders withSpinner
mod_import_feature_ui <- function(id, r_global) {
  ns <- NS(id)
  tagList(
    div(
      id = id,
      mod_side_description_ui(ns("module_title"), r_global, "mod_import_feature", show_elements = c("project")),
      fluidRow(
        box(
          width = 3, # 7,
          id = "param_set", # status = "primary",


          column(
            width = 12,
            textInput(ns("comment"), "Description of the dataset to be loaded :", "")
          ),
          fluidRow(
            column(width = 2),
            column(
              width = 8, uiOutput(ns("createFE_button"))
            ), column(width = 2)
          )
        ),
        column(
          width = 9,
          tabBox(
            width = 12, side = "right",
            title = "Features Loading",
            id = "feat_load",
            tabPanel("Load",
              id = "first_page",
              tagList(
                fluidRow(
                  box(
                    status = "primary",
                    width = 6, title = span(icon("clipboard-list"), "Abundance table"),
                    solidHeader = FALSE,
                    datamods::import_file_ui(
                      title = "Abundance", ns("import-abundance"),
                      file_extensions = c(".csv", ".txt", ".xls", ".xlsx"),
                      preview_data = F
                    ),
                    withSpinner(DT::dataTableOutput(outputId = ns("data_abun")), type = 6),
                    br(),
                    fluidRow( ## fluidrow abundance ####
                      column(
                        width = 6,
                        tags$div(class = "InputSized", uiOutput(ns("sharedColAb_UI")))
                      ),
                      column(
                        width = 6, tags$br(),
                        tags$div(class = "InputSized", uiOutput(ns("percent_UI")))
                      )
                    )
                  ),
                  box(
                    width = 6, status = "primary",
                    title = span(icon("clipboard-list"), "MFIs table"),
                    solidHeader = FALSE,
                    datamods::import_file_ui(
                      title = "MFIs", ns("import-mfis"), preview_data = F,
                      file_extensions = c(".csv", ".txt", ".xls", ".xlsx")
                    ),
                    withSpinner(DT::dataTableOutput(outputId = ns("data_mfi")), type = 6),
                    br(),
                    fluidRow( ## fluidrow mfis ####
                      column(
                        width = 5,
                        tags$div(class = "InputSized", uiOutput(ns("sharedColMFI_UI"))) # "id_col"
                      ),
                      column(
                        width = 3, tags$br(),
                        tags$div(class = "InputSized", uiOutput(ns("is_scaled_mfi"))) # true or false
                      ),
                      column(
                        width = 4, tags$br(),
                        tags$div(class = "InputSized", uiOutput(ns("to_scale_mfi")))
                      )
                    ),
                    fluidRow(tags$div(uiOutput(ns("conditionnal_asinh_MFI")))),
                    tags$div(uiOutput(ns("conditional_long_mfi"))) # if format mfi is long : need value mfi column name, cluter c name, marker c name
                  )
                ),
                fluidRow( ## metadata ####
                  box(
                    width = 6, status = "primary",
                    title = span(icon("clipboard-list"), "Metadata table"),
                    solidHeader = FALSE,
                    datamods::import_file_ui(
                      title = "Metadata", ns("import-metadata"), preview_data = F,
                      file_extensions = c(".csv", ".txt", ".xls", ".xlsx")
                    ),
                    withSpinner(DT::dataTableOutput(outputId = ns("data_metadata")), type = 6),
                    br(),
                    fluidRow( ## fluidrow mfis ####
                      column(
                        width = 6,
                        tags$div(class = "InputSized", uiOutput(ns("sharedColMeta_UI"))) # "id_col"
                      ),
                      column(
                        width = 6,
                        tags$div(class = "InputSized", uiOutput(ns("column_ncells")))
                      ),
                      column(
                        width = 6,
                        tags$div(class = "InputSized", uiOutput(ns("change_metadata_column")))
                      ),
                      column(
                        width = 6,
                        tags$div(class = "InputSized", uiOutput(ns("conditionnal_other_name")))
                      )
                    )
                  ),
                  box(
                    width = 6, status = "primary",
                    title = span(icon("clipboard-list"), "Markers table"),
                    solidHeader = FALSE,
                    align = "center",
                    uiOutput(ns("future_table_markers"))
                  )
                )
              )
            )
          )
        )
      )
    )
  )
}

#' import_feature Server Functions
#'
#' @importFrom fs path
#' @importFrom shinyWidgets updatePickerInput pickerInput
#' @noRd
mod_import_feature_server <- function(id, r_global, reloadTrigger) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns

    state_load <- reactiveVal(FALSE)
    one_is_loaded <- reactiveVal(FALSE)
    feature_values_load <- reactiveVal(FALSE)
    data_downloaded <- reactiveVal(FALSE)
    dataFE <- reactiveVal()


    mod_side_description_server("module_title", module_name = "mod_import_feature", alert = FALSE)

    observe({
      how <- "success" %in% c(
        imported_abun$status(),
        imported_mfi$status(),
        imported_metadata$status()
      )
      one_is_loaded(how)
    })

    observe({
      how <- "success" %in% c(
        imported_abun$status(),
        imported_mfi$status()
      ) & "success" %in% c(imported_metadata$status())

      state_load(how)
    })




    observe({
      how <- "success" %in% c(
        imported_abun$status(),
        imported_mfi$status()
      )
      feature_values_load(how)
    })


    defaultValue <- reactiveValues(
      clustCol = "filter",
      markerCol = "ftr (sec)",
      mfiValueCol = "value",
      sharedCol = "file",
      totalCell = "Unfiltered|count",
      # file_format_abund = "wide",
      # file_format_mfi = "long",
      fcs_names = "patient",
      ppatient = 1,
      pcluster = 1,
      is_scaled = T,
      to_scaled = F,
      fcs_to_cut = F,
      alternative_name = F
    )


    imported_abun <- datamods::import_file_server(
      id = "import-abundance",
      show_data_in = "modal",
      return_class = "data.frame",
      trigger_return = "change"
    )

    imported_mfi <- datamods::import_file_server(
      id = "import-mfis",
      show_data_in = "modal",
      return_class = "data.frame",
      trigger_return = "change"
    )

    imported_metadata <- datamods::import_file_server(
      id = "import-metadata",
      show_data_in = "modal",
      return_class = "data.frame",
      trigger_return = "change"
    )

    output$percent_UI <- renderUI({
      req(imported_abun$data())
      shinyWidgets::awesomeCheckbox(ns("check_rawPercent"), status = "warning", label = HTML("<b>is in percent<b>"), value = FALSE)
    })

    output$sharedColAb_UI <- renderUI({
      req(imported_abun$data())
      pickerInput(inputId = ns("sharedColAbundance"), "Shared column:", choices = colnames(imported_abun$data()), selected = defaultValue$sharedCol)
    })

    output$future_table_markers <- renderUI({
      req(imported_mfi$data())

      column(
        width = 12,
        mod_type_state_table_ui(ns("show_table_markers_id"))
      )
    })




    output$data_abun <- DT::renderDataTable(
      imported_abun$data(),
      escape = FALSE,
      selection = "none", server = FALSE,
      options = list(dom = "t", scrollX = TRUE, ordering = FALSE)
    )

    output$data_mfi <- DT::renderDataTable(
      imported_mfi$data(),
      escape = FALSE,
      selection = "none", server = FALSE,
      options = list(dom = "t", scrollX = TRUE, ordering = FALSE)
    )

    output$data_metadata <- DT::renderDataTable(
      imported_metadata$data(),
      escape = FALSE,
      selection = "none", server = FALSE,
      options = list(dom = "t", scrollX = TRUE, ordering = FALSE)
    )

    # datamods::import_file_server("import-mfis", trigger_return = "change")

    output$sharedColMFI_UI <- renderUI({
      req(imported_mfi$data())
      shinyWidgets::pickerInput(inputId = ns("sharedColMFI"), "Shared column:", choices = colnames(imported_mfi$data()), selected = defaultValue$sharedCol)
    })

    output$to_scale_mfi <- renderUI({
      req(imported_mfi$data())
      shinyWidgets::awesomeCheckbox(ns("check_is_need_scale"), status = "warning", label = HTML("<b>need to scale<b>"), value = defaultValue$to_scaled)
    })

    output$is_scaled_mfi <- renderUI({
      req(imported_mfi$data())
      shinyWidgets::awesomeCheckbox(ns("check_is_scaled_mfi"), status = "warning", label = HTML("<b>is scaled<b>"), value = defaultValue$is_scaled)
    })

    output$sharedColMeta_UI <- renderUI({
      req(imported_metadata$data())
      shinyWidgets::pickerInput(inputId = ns("sharedColMeta"), "Shared column:", choices = colnames(imported_metadata$data()), selected = defaultValue$sharedCol)
    })



    output$change_metadata_column <- renderUI({
      req(imported_metadata$data())
      shinyWidgets::awesomeCheckbox(ns("enable_alternative_name"), status = "warning", label = HTML("<b>Alternative name:<b>"), value = defaultValue$alternative_name)
    })


    output$conditionnal_other_name <- renderUI({
      req(imported_metadata$data())

      metadata <- imported_metadata$data()
      unique_columns <- colnames(metadata)[sapply(metadata, function(col) length(unique(col)) == nrow(metadata))]

      tagList(
        # fluidRow(
        conditionalPanel(
          condition = "input.enable_alternative_name",
          shinyWidgets::pickerInput(
            inputId = ns("sample_rename"), # ns("sample_rename"),
            label = "Alternative name:",
            choices = unique_columns
          ),
          ns = ns
        )
        # )
      )
    })


    observeEvent(input$enable_alternative_name, {
      if (!input$enable_alternative_name) {
        updatePickerInput(session, inputId = "sample_rename", selected = NULL)
      }
    })


    output$column_ncells <- renderUI({
      req(imported_metadata$data())

      numeric_cols <- colnames(imported_metadata$data())[sapply(imported_metadata$data(), is.numeric)]
      pickerInput(
        inputId = ns("num_cells_col"), "Number of cells column:", choices = numeric_cols,
        selected = defaultValue$totalCell
      )
    })

    ### MFIS ####


    output$conditionnal_asinh_MFI <- renderUI({
      req(imported_mfi$data())


      tagList(
        # fluidRow(
        column(width = 4, conditionalPanel(
          condition = "input.check_is_need_scale",
          numericInput(inputId = ns("num_asinh_MFI"), "Chose asinh value:", value = 5, step = 1),
          ns = ns
        ))
        # )
      )
    })

    output$conditional_long_mfi <- renderUI({
      req(imported_mfi$data())

      tagList(
        fluidRow(
          column(
            width = 4,
            pickerInput(
              ns("mfi_col_sample"),
              label = "Sample name:", # old fcs
              choices = colnames(imported_mfi$data()),
              selected = defaultValue$sharedCol
            )
          ),
          column(
            width = 4,
            pickerInput(
              ns("mfi_col_cluster"),
              label = "Cluster:",
              choices = colnames(imported_mfi$data()),
              selected = defaultValue$clustCol
            )
          ),
          column(
            width = 4,
            pickerInput(
              ns("mfi_col_marker"),
              label = "Marker:",
              choices = colnames(imported_mfi$data()),
              selected = defaultValue$markerCol
            )
          ),
          column(
            width = 4,
            pickerInput(
              ns("mfi_col_mfi"),
              label = "MFI value:", # ex Value
              choices = colnames(imported_mfi$data()),
              selected = defaultValue$mfiValueCol
            )
          )
        )
      )
    })

    # datamods::import_file_server("import-metadata", trigger_return = "change")

    ### MARKERS ####

    param_list <- reactive({
      obj <- list(
        "list_formats" = list(
          "abundance" = "wide", # "abundance" = input$format_abund,
          "mfi" = "long", # "mfi" =  input$format_mfi,
          "metadata" = "wide", # "metadata" = input$format_metadata,
          "markers" = NULL
        ),
        "grouping_colname" = list(
          "abundance" = input$sharedColAbundance,
          "mfi" = input$sharedColMFI,
          "metadata" = input$sharedColMeta,
          "markers" = NULL
        ),
        "parsing" = list(
          sep = "#",
          pos_ids = 1
        ),
        "target_long_columns" = list(
          "value" = input$mfi_col_sample,
          "cluster" = input$mfi_col_cluster,
          "marker" = input$mfi_col_marker,
          "mfi" = input$mfi_col_mfi
        ),
        "fcs_ids" = "patient", #        "fcs_ids" = input$fcs_names,
        # "abundance" = imported_abun$data() |> dplyr::arrange(!!sym(input$sharedColAbundance)),
        "abundance" = imported_abun$data(),
        "mfi" = imported_mfi$data(),
        "is_scaled" = input$check_is_scaled_mfi,
        "to_scale" = input$check_is_need_scale,
        # "meta" = imported_metadata$data() |> dplyr::arrange(!!sym(input$sharedColMeta)),
        "meta" = imported_metadata$data(),
        "color" = NULL,
        "markers" = NULL,
        "applied_fct" = "asinh", #         "applied_fct" =  input$transformation_fct,
        "asinh_Ab" = 0.03, #         "asinh_Ab" = input$Ab_aSinh,
        "fcs_split" = FALSE, #        "fcs_split" = input$check_remove_fcs_labs,
        "is_percent" = input$check_rawPercent,
        "asinh_MFI" = input$num_asinh_MFI,
        # "state_marker" = NULL,
        "omiq_column_name" = TRUE, #        "omiq_column_name" = input$check_is_colname_omiq,
        "column_with_ncells" = input$num_cells_col,
        "sample_rename" = input$sample_rename,
        "annot_markers" = table_markers()
      )

      return(obj)
    })


    output$params_object_see <- renderPrint({
      req(param_list())
      print(param_list())
    })

    # launch SE with features tables

    output$createFE_button <- renderUI({
      req(state_load())

      tagList(
        fluidRow(
          column(12,
            align = "center",
            actionBttn(
              inputId = ns("runFE"),
              class = "btn-secondary",
              icon = icon("gears"),
              style = "material-flat",
              label = "Create a feature object(5: fe)"
              # , label = "Create the Analysis object"
            )
          )
        )
      )
    })

    fcs_filt_origin_marker <- reactiveVal()
    vector_markers <- reactiveVal()
    vector_clusters <- reactiveVal()
    table_markers <- reactiveVal(NULL)


    observe({
      req(imported_mfi$data())
      req(input$mfi_col_cluster)
      value_mfi_cluster <- imported_mfi$data()[, input$mfi_col_cluster]
      vector_clusters(unique(value_mfi_cluster))
    })


    observe({
      req(imported_mfi$data())
      req(input$mfi_col_marker)
      req(input$mfi_col_mfi)
      req(input$mfi_col_cluster)

      col_mfi_marker <- imported_mfi$data()[, input$mfi_col_marker]
      col_mfi_cluster <- imported_mfi$data()[, input$mfi_col_cluster]
      value_mfi_marker <- imported_mfi$data()[, input$mfi_col_mfi]



      formula_metacluster <- stats::as.formula(paste0("`", input$mfi_col_mfi, "`", "~", "`", input$mfi_col_marker, "`", "+", "`", input$mfi_col_cluster, "`"))

      req(class(col_mfi_marker) != "numeric")
      req(class(col_mfi_cluster) != "numeric")
      req(class(value_mfi_marker) == "numeric")


      median_data <- stats::aggregate(
        formula_metacluster,
        data = imported_mfi$data(),
        FUN = median
      )

      formula <- stats::as.formula(paste0("value", "~", "`", input$mfi_col_marker, "`"))


      if (input$check_is_need_scale) {
        min_max_matrix <- stats::aggregate(formula,
          data = median_data,
          FUN = function(x) c(min = asinh(min(x) / input$num_asinh_MFI), max = asinh(max(x) / input$num_asinh_MFI))
        )
      } else {
        min_max_matrix <- stats::aggregate(
          formula,
          data = median_data,
          FUN = function(x) c(min = min(x), max = max(x))
        )
      }

      min_max_matrix <- do.call(data.frame, min_max_matrix |> dplyr::arrange(input$mfi_col_marker))
      colnames(min_max_matrix) <- c("marker", "min value", "max value")


      # print(min_max_matrix)
      vector_markers(sort(unique(col_mfi_marker)))

      myreturn <- callModule(
        mod_type_state_table_server,
        "show_table_markers_id",
        markers = vector_markers(),
        min_max_tb = min_max_matrix,
        height_Y = NULL
        # min_max_tb = NULL
      )

      observeEvent(myreturn$results,
        {
          # browser()
          # Create a data frame with the combined results
          df <- data.frame(
            check.names = F,
            # desc = names(myreturn$results$states),
            # marker_class = myreturn$results$states, # annot_markers = myreturn$results$states,
            `min value` = myreturn$results$min_values,
            `max value` = myreturn$results$max_values
          )

          # print(df)

          interest_marker_vector <- myreturn$results$states

          fcs_filt_origin_marker(interest_marker_vector)

          # rownames(df) <- names(myreturn$results$states)
          table_markers(df)
        },
        ignoreNULL = F
      )
    })

    observeEvent(input$runFE, {
      if (nrow(table_markers()) != length(vector_markers())) {
        notification_failure("Error: Missing 'type' or 'state' references for some markers.", title = "error")
      }
    })



    observeEvent(input$runFE, {
      req(input$mfi_col_cluster)
      if (ncol(imported_abun$data()) - 1 != length(vector_clusters())) { # don't check ids but length at least
        notification_failure(paste0("Error: Cluster counts differ in 'abundance' vs. mfi ", input$mfi_col_cluster, " column."), title = "error")
      }
    })


    req_column_metadata_ncell <- reactiveVal(FALSE)

    observe({
      if (!is.null(input$num_cells_col) && input$num_cells_col != "") {
        req_column_metadata_ncell(TRUE) # Requirement is met
      } else {
        req_column_metadata_ncell(FALSE) # Requirement is not met
      }
    })


    observeEvent(input$runFE, {
      if (!req_column_metadata_ncell()) {
        notification_failure("Please define a metadata column with the number of cells.", title = "Error")
      }
    })

    fcs_list_abun <- reactiveVal()
    fcs_list_mfi <- reactiveVal()
    fcs_list_meta <- reactiveVal()

    metadata_cell_column <- reactiveVal()

    abundance_cluster_ncells <- reactiveVal()


    observe({
      req(imported_metadata$data(), input$sharedColMeta)
      fcs_list_meta(sort(as.character(unique(imported_metadata$data()[[input$sharedColMeta]]))))
    })


    observe({
      req(imported_abun$data(), input$sharedColAbundance)
      fcs_list_abun(sort(as.character(unique(imported_abun$data()[[input$sharedColAbundance]]))))
    })

    observe({
      req(imported_metadata$data(), input$num_cells_col, input$sharedColMeta)
      metadata_cell_column(setNames(imported_metadata$data()[[input$num_cells_col]], imported_metadata$data()[[input$sharedColMeta]]))
    })

    observe({
      req(imported_abun$data(), input$sharedColAbundance)

      abun_data <- imported_abun$data()

      numeric_cols <- setdiff(colnames(abun_data), input$sharedColAbundance)
      valid_data <- abun_data[, numeric_cols, drop = FALSE]
      req(all(sapply(valid_data, is.numeric))) # Ensure all columns are numeric

      result <- setNames(
        rowSums(valid_data),
        abun_data[[input$sharedColAbundance]]
      )

      abundance_cluster_ncells(result)
    })


    number_of_cell_nonconcordant <- reactiveVal()

    observeEvent(input$runFE, {
      req(metadata_cell_column(), abundance_cluster_ncells())
      if (!input$check_rawPercent) {
        number_of_cell_nonconcordant(!all(names(metadata_cell_column()) %in% names(abundance_cluster_ncells())) ||
          !all(metadata_cell_column()[names(abundance_cluster_ncells())] == abundance_cluster_ncells()))

        # Check for concordance
        if (number_of_cell_nonconcordant()) {
          notification_warning("Number of cells are not concordant.", title = "Warning")
        }
      }
    })

    observe({
      req(imported_mfi$data(), input$sharedColMFI)
      fcs_list_mfi(sort(as.character(unique(imported_mfi$data()[[input$sharedColMFI]]))))
    })


    observeEvent(input$runFE, {
      comparisons <- list(
        list("metadata and mfi", fcs_list_meta(), fcs_list_mfi()),
        list("metadata and abundance", fcs_list_meta(), fcs_list_abun()),
        list("abundance and mfi", fcs_list_abun(), fcs_list_mfi())
      )

      for (comp in comparisons) {
        test_diff <- symmetric_diff(comp[[2]], comp[[3]])
        if (length(test_diff) > 0) {
          notification_failure(
            paste0(
              "Error: ", paste0(test_diff, collapse = ","),
              " differs between ", comp[[1]], " files."
            ),
            title = "error"
          )
        }
      }
    })


    observeEvent(input$runFE, {
      if (length(fcs_filt_origin_marker()) == 0) {
        notification_failure("Error: Please select type/state markers.", title = "error")
      }
    })



    observeEvent(input$runFE, {
      # browser()
      req(fcs_filt_origin_marker())
      req(nrow(table_markers()) == length(vector_markers()))
      req(ncol(imported_abun$data()) - 1 == length(vector_clusters()))

      req(all(Vectorize(identical, "x")(list(
        fcs_list_meta(),
        fcs_list_abun(),
        fcs_list_mfi()
      ), fcs_list_meta())))

      req(req_column_metadata_ncell())
      # req(!number_of_cell_nonconcordant())

      state_marker <- analycyte.utils::create_long_marker_table(fcs_filt_origin_marker())

      r_global$data <- analycyte.utils::create_se( # presuppose que les tables sont dans le meme ordre, a modifier
        list_formats = param_list()[["list_formats"]],
        abundance = param_list()[["abundance"]],
        mfi = param_list()[["mfi"]],
        is_scaled = param_list()[["is_scaled"]],
        to_scale = param_list()[["to_scale"]],
        meta = param_list()[["meta"]],
        color = param_list()[["color"]],
        markers = param_list()[["markers"]],
        target_long_columns = param_list()[["target_long_columns"]],
        fcs_ids = param_list()[["fcs_ids"]],
        parsing = param_list()[["parsing"]],
        grouping_colname = param_list()[["grouping_colname"]],
        applied_fct = param_list()[["applied_fct"]],
        asinh_Ab = param_list()[["asinh_Ab"]],
        column_with_ncells = param_list()[["column_with_ncells"]],
        fcs_split = param_list()[["fcs_split"]],
        is_percent = param_list()[["is_percent"]],
        asinh_MFI = param_list()[["asinh_MFI"]],
        # state_marker = param_list()[["state_marker"]],
        state_marker = state_marker,
        sample_rename = param_list()[["sample_rename"]],
        omiq_column_name = param_list()[["omiq_column_name"]],
        annot_markers = param_list()[["annot_markers"]]
      )

      notification_success("FE loaded", title = "success")
      reloadTrigger(reloadTrigger() + 1)

      new_step(project = r_global$project_id, label = "mod_import_feature", output_type = "fe", comment = input$comment)

      last_SID <- steps() |>
        dplyr::pull(.data$SID) |>
        max()

      step_row <- steps() |> dplyr::filter(.data$SID == last_SID)
      project_row <- projects(verbose = T) |> dplyr::filter(.data$id == step_row$PID)

      # recuperation du hash_tag

      hash <- step_row |> dplyr::pull(.data$hash_tag)
      project_path <- project_row |> dplyr::pull(.data$path)

      path <- unclass(fs::path(project_path, hash))

      # ecriture du data sur le disque

      if (dir.exists(path)) {
        saveRDS(r_global$data, file = file.path(path, "dataFE.rds"))
        notification_success("RDS saved", title = "success")

        reloadTrigger(reloadTrigger() + 1)
      } else {
        notification_warning("No project hash folder found.", title = "warning")
        reloadTrigger(reloadTrigger() + 1)
      }

      finish_step(project = r_global$project_id, SID = last_SID)
    })
  })
}
