#' redefine_fe UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
mod_create_variable_ui <- function(id, r_global) {
  ns <- NS(id)
  tagList(
    div(
      id = id,
      mod_side_description_ui(ns("module_title"), r_global, "mod_create_variable", show_elements = c("project", "folder")),
      fluidRow(
        box(
          # height = 200,
          width = 3, # 7,
          id = "param_set",
          column(
            width = 12,
            textInput(ns("comment"), "Description on the new annotated set:", "")
          ),
          fluidRow(
            column(
              width = 12, align = "center",
              actionBttn(
                inputId = ns("save_fe"),
                class = "btn-secondary",
                icon = icon("gears"),
                style = "material-flat",
                label = "save feature"
              )
            )
          )
        ),
        column(
          width = 9,
          featuresBoxesUI(ns("features_boxes")),
          tags$br(),
          fluidRow(
            actionButton(ns("refresh"), "Reset", class = "btn-dir reload", icon = icon("arrows-rotate"))
          )
        )
      ),
      fluidRow(
        column(
          width = 12,
          div(DT::dataTableOutput(ns("fcs_info")), class = "table1")
        )
      ), tags$br(),

      # fluidRow(
      #   box(
      #     width = 7, status = "primary", solidHeader = F, title = "New column definition",
      #     datamods::create_column_ui(ns("create_col"))
      #   )
      # )

      fluidRow(
        box(
          width = 6, status = "primary", solidHeader = F, title = "Redefine variable class",
          datamods::update_variables_ui(ns("fcs_vars"))
        ),
        # box(
        #   width = 5, status = "primary", solidHeader = F, title = "Define group from numeric",
        #   datamods::cut_variable_ui(ns("mod_cut"))
        # ), tags$br(),
        box(
          width = 6, status = "primary", solidHeader = F, title = "New column definition",
          datamods::create_column_ui(ns("create_col"))
        )
      )
    )
  )
}

#' redefine_fe Server Functions
#'
#' @noRd
mod_create_variable_server <- function(id, r_global, reloadTrigger) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    savedFilterValues <- reactiveVal()


    mod_side_description_server("module_title", module_name = "mod_create_variable", alert = TRUE)


    dataFE <- reactiveVal()
    Folder_Name <- reactiveVal()
    Project_Id <- reactiveVal()
    dataFE(r_global$data)
    Folder_Name(r_global$project_folder_name)
    Project_Id(r_global$project_id)

    observeEvent(input$faq_btn, {
      showModal(modalDialog(
        pages_description[[associated_name[["mod_create_variable"]]]],
        easyClose = TRUE,
        footer = NULL
      ))
    })


    fcs_data <- reactive({
      req(dataFE())
      return(dataFE()$SE_abundance@colData |> as.data.frame())
    })

    rv <- reactiveValues(
      data = fcs_data()
    )


    # Control flag usage during reset
    observeEvent(input$refresh,
      {
        rv$data <- isolate(dataFE()$SE_abundance@colData |> as.data.frame())
      },
      ignoreInit = TRUE
    )

    row_data <- reactive({
      req(dataFE())
      return(getFeature(dataFE()$SE_abundance, c("rowData")) |> as.data.frame())
    })

    markers_data <- reactive({
      req(dataFE())
      if ("annot_markers" %in% names(dataFE()$SE_mfi@metadata)) {
        return(dataFE()$SE_mfi@metadata$annot_markers |> as.data.frame())
      } else {
        return(data.frame(desc = names(getFeature(dataFE()$SE_mfi, c("assays")))))
      }
    })

    counts_table <- reactive({
      req(dataFE())

      return(getFeature(dataFE()$SE_abundance, c("assays", "counts")) |> as.data.frame())
    })

    featuresBoxesServer("features_boxes", width_ = 3, fcs_data, row_data, markers_data, counts_table)

    # this will stop execution if fcs_data() is NULL or empty

    updated_fcs_data <- datamods::update_variables_server(
      id = "fcs_vars",
      # data = reactive({
      #   rv$data
      # })
      data = rv$data
    )


    data_mod_cut_r <- datamods::cut_variable_server(
      id = "mod_cut",
      data_r = reactive(rv$data)
    )

    data_col_r <- datamods::create_column_server(
      id = "create_col",
      data_r = reactive(rv$data)
    )

    observeEvent(updated_fcs_data(),
      {
        new_data <- updated_fcs_data()

        rv$data <- new_data
      },
      ignoreInit = TRUE
    )

    observeEvent(data_mod_cut_r(),
      {
        new_data <- data_mod_cut_r()

        rv$data <- new_data
      },
      ignoreInit = TRUE
    )


    observeEvent(data_col_r(),
      {
        new_data <- data_col_r()
        rv$data <- new_data
      },
      ignoreInit = TRUE
    )

    output$fcs_info <- DT::renderDataTable(
      {
        prettyDT(rv$data,
          extensions_ = list(),
          rownames = FALSE,
          orderable_ = TRUE,
          selection = "none",
          caption = "FCS information table"
        )
      },
      server = F
    )
  })
}
